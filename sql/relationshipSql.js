var constant = require('../constants/constants');
var helper = require('../util/helper'); //general tools 
const QueryBuilder = require('node-querybuilder');
const settings = constant.dbConn;
const pool = new QueryBuilder(settings, 'mysql', 'pool');

exports.tryLink = async function (teacher, student) {
    const qb = await pool.get_connection();
    try {
        const response = await qb.select('student_id').where({ 'teacher_id =': teacher, student_id: student }).get('relationship');
        if (response.length > 0) {
            var exitsStudentId = [];
            for (const resStudent of response) {
                exitsStudentId.push(resStudent.student_id);
            }
            student = student.filter(function (val) {
                return exitsStudentId.indexOf(val) == -1;
            });
        }
        if (student.length > 0) {
            var data = [];
            for (const student_id of student) {
                data.push({ teacher_id: teacher, student_id: student_id });
            }
            await qb.insert_batch('relationship', data);
        }


    } catch (err) {
        return console.error("Uh oh! Couldn't get results: " + err.msg);
    } finally {
        qb.release();
    }

}

exports.getStudentNameByTeacher = async function (teacher) {
    const qb = await pool.get_connection();
    try {
        if (helper.isArray(teacher)) {

            var response = await qb.distinct().select(['s.email', 'count(s.email) as duplicate']).from('student s')
                .join('relationship r', 'r.student_id = s.id', 'inner', true)
                .join('teacher t', 't.id = r.teacher_id', 'inner', true)
                .where({ 't.email': teacher })
                .group_by('s.email')
                .having('duplicate > 1')
                .get();
        } else if (helper.isString(teacher)) {
          
            var response = await qb.distinct().select('s.email').from('student s')
                .join('relationship r', 'r.student_id = s.id', 'inner', true)
                .join('teacher t', 't.id = r.teacher_id', 'inner', true)
                .where({ 't.email': teacher })
                .get();
        }

        var result = [];
        if (response.length > 0) {
            for (const student of response) {
                result.push(student.email);
            }
        }
        return result;
    } catch (err) {
        return console.error("Uh oh! Couldn't get results: " + err.msg);
    } finally {
        qb.release();
    }

}
