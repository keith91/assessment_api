var constant = require('../constants/constants');
const QueryBuilder = require('node-querybuilder');
const settings = constant.dbConn;
const pool = new QueryBuilder(settings, 'mysql', 'pool');

exports.tryInsert = async function (param) {
    const qb = await pool.get_connection();
    try {
        const response = await qb.select('id').where({ 'email =': param }).get('student');
        if (response.length > 0) {
            return response[0].id;
        } else if (response.length == 0) {
            const response = await qb.returning('id').insert('student', { email: param });
            return response.insertId;
        }
    } catch (err) {
        return console.error("Uh oh! Couldn't get results: " + err.msg);
    } finally {
        qb.release();
    }

}

exports.suspendStudent = async function (param) {
    const qb = await pool.get_connection();
    try {
        await qb.update('student', { status: 0 }, { 'email': param });
    } catch (err) {
        return console.error("Uh oh! Couldn't get results: " + err.msg);
    } finally {
        qb.release();
    }

}
