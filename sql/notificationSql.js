var constant = require('../constants/constants');
const QueryBuilder = require('node-querybuilder');
const settings = constant.dbConn;
const pool = new QueryBuilder(settings, 'mysql', 'pool');
var helper = require('../util/helper'); //general tools 

exports.sendNotification = async function (sender, message, teacherId) {
    const qb = await pool.get_connection();
    try {
        var validReciepient = [];

        var taggedEmail = [];
        var splitMsg = message.split(' ');

        // get tagged email
        if (splitMsg.length > 0) {
            for (const splitMsgContent of splitMsg) {
                if (splitMsgContent.charAt(0) == "@" && helper.isEmail(splitMsgContent.slice(1))) {
                    taggedEmail.push(splitMsgContent.slice(1));
                }
            }
        } else {
            if (message.charAt(0) == "@" && helper.isEmail(message.slice(1))) {
                taggedEmail.push(message.slice(1));
            }
        }

        // get origin valid reciepient
        validReciepient = await qb.select(['s.id', 's.email']).from('student s')
            .join('relationship r', 'r.student_id = s.id', 'inner', true)
            .join('teacher t', 't.id = r.teacher_id', 'inner', true)
            .where({ 's.status = ': '1', 't.email': sender })
            .get();

        // join and de-duplicate both array
        var tagReciepient = [];
        if (taggedEmail.length > 0) {
            tagReciepient = await qb.select('id, email').where({ 'status =': 1, email: taggedEmail }).get('student');
        }

        var totalReciepient = tagReciepient.concat(validReciepient);
        validReciepient = totalReciepient;

        var totalReciepientId = [];
        for (const student of validReciepient) {
            totalReciepientId.push(student.id);
        }
        totalReciepientId = arrayUnique(totalReciepientId);

        // check existing notification
        const existingNotification = await qb.select('n.*').from('notification n')
            .join('teacher t', 't.id = n.sender', 'inner', true)
            .where({ 't.email = ': sender, 'n.message = ': message })
            .get();

        if (validReciepient.length > 0) {
            if (existingNotification.length > 0) {
                // do see haven recieve notification yet
    
                var existingRecipient = existingNotification[0].recipient.split(",").map(function (x) {
                    return parseInt(x);
                });;
                var newRecipientId = arr_diff(existingRecipient, totalReciepientId);
                var newTotalReciepientId = existingRecipient.concat(newRecipientId);
         
                
                await qb.update('notification', { recipient: newTotalReciepientId.toString() }, { 'id': existingNotification[0].id });
            } else {
                // save new notification
                await qb.insert('notification', { sender: teacherId, message: message, recipient: totalReciepientId.toString() });
            }
        }

        var result = [];
        for (const student of validReciepient) {
            result.push(student.email);
        }

        return arrayUnique(result);
    } catch (err) {
        return console.error("Uh oh! Couldn't get results: " + err);
    } finally {
        qb.release();
    }

}

function arrayUnique(array) {
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
}

function arr_diff(a1, a2) {

    var a = [], diff = [];
    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }
    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }
    for (var k in a) {
        diff.push(parseInt(k));
    }

    return diff;
}