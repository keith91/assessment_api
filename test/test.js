var request = require('supertest');
var app = require('../server');

describe('POST /api/register (normal register togehter with student)', function() {
  it('responds with no content', function(done) {
    request(app)
      .post('/api/register')
      .send({teacher : "teacherKeith@gmail.com", students : ["student1@gmaill.com","student2@gmaill.com"]})
      .set('Accept', 'application/json')
      .expect(204).end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        done();
      });;
  });
});

describe('POST /api/register (register other teacher but same student)', function() {
  it('responds with no content', function(done) {
    request(app)
      .post('/api/register')
      .send({teacher : "teacherSam@gmail.com", students : ["student1@gmaill.com","student2@gmaill.com","student3@gmaill.com"]})
      .set('Accept', 'application/json')
      .expect(204).end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        done();
      });;
  });
});

describe('POST /api/register (without teacher field)', function() {
  it('responds with error', function(done) {
    request(app)
      .post('/api/register')
      .send({students : ["student1@gmaill.com"]})
      .set('Accept', 'application/json')
      .expect(422)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });
  });
});

describe('POST /api/register (without students field)', function() {
  it('responds with error', function(done) {
    request(app)
      .post('/api/register')
      .send({teacher : "teacherKeith@gmail.com"})
      .set('Accept', 'application/json')
      .expect(422)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });
  });
});

describe('POST /api/register (invalid teacher email)', function() {
  it('responds with error', function(done) {
    request(app)
      .post('/api/register')
      .send({teacher : "teacherKeith@gmail", students : ["student1@gmaill"]})
      .set('Accept', 'application/json')
      .expect(422)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });;
  });
});

describe('POST /api/register (invalid student email)', function() {
  it('responds with error', function(done) {
    request(app)
      .post('/api/register')
      .send({teacher : "teacherKeith@gmail.com", students : ["student1@gmaill","student2@gmail.com"]})
      .set('Accept', 'application/json')
      .expect(422)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });;
  });
});

describe('GET /api/commonstudents (one teacher)', function() {
  it('responds with json', function(done) {
    request(app)
      .get('/api/commonstudents?teacher=teacherKeith@gmail.com')
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });;
  });
});

describe('GET /api/commonstudents (two teacher)', function() {
  it('responds with json', function(done) {
    request(app)
      .get('/api/commonstudents?teacher=teacherKeith@gmail.com&teacher=teacherSam@gmail.com')
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });;
  });
});

describe('GET /api/commonstudents (without teacher)', function() {
  it('responds with json', function(done) {
    request(app)
      .get('/api/commonstudents')
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });;
  });
});

describe('POST /api/suspend (email: student1@gmaill.com)', function() {
  it('responds with no content', function(done) {
    request(app)
      .post('/api/suspend')
      .send({student : "student1@gmaill.com"})
      .set('Accept', 'application/json')
      .expect(204)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        done();
      });;
  });
});

describe('POST /api/retrievefornotifications (with @mentions student3@gmaill.com which not under this teacher ; Take note : student1@gmaill.com is suspended)', function() {
  it('responds with json', function(done) {
    request(app)
      .post('/api/retrievefornotifications')
      .send({teacher : "teacherKeith@gmail.com", notification : "Hello students! @student3@gmaill.com"})
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });;
  });
});

describe('POST /api/retrievefornotifications (without @mentions ; Take note : student1@gmaill.com is suspended)', function() {
  it('responds with json', function(done) {
    request(app)
      .post('/api/retrievefornotifications')
      .send({teacher : "teacherKeith@gmail.com", notification : "Hello students!"})
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        console.log("response code",res.status)
        console.log(res.text)
        done();
      });;
  });
});