'use strict';
module.exports = {
  missingRequestInfo: "Invalid request body information, please refer to api document!",
  serverError: "oops! something is wrong, please try again later",
  invalidEmailFormat: " is not a valid email!",
  invalidTeacher: "Teacher account not found",
  dbConn: {
    host: '<your host here>', //db host here
    user: '<your username here>',      //db username here
    password: '<your password here>', //db password here
    database: '<your database name here>' //db name here
  }
};
