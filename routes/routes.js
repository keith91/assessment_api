'use strict';
module.exports = function(app) {
  var teacherCtrl = require('../controllers/teacherController');

  app.route('/api/register').post(teacherCtrl.register);
  app.route('/api/commonstudents').get(teacherCtrl.commonStudents);
  app.route('/api/suspend').post(teacherCtrl.suspendStudent);
  app.route('/api/retrievefornotifications').post(teacherCtrl.retrieveForNotifications);

};
