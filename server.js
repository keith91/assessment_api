const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());
const port = 8080;
var routes = require('./routes/routes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);

// return error message if url not fount
app.use(function (req, res) {
    res.status(404).json({ message: req.method + ' : ' + req.headers.host + req.originalUrl + ' URL not found' })
});

app.listen(port, () => { console.log('api server listening on port ' + port); });

module.exports = app;