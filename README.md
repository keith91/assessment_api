# assessment_api

how to run : 
- port 8080 will occupied by this program, if you want to change the port , u can change it on `server.js`
- run on the enviroment with npm installed
- go to the document root
- run "`npm install`" to install the dependencies
- execute the `database.sql` which locate in the document root, run the script on your local database
- after install, change the "`constant > constant.js`" file and change the database dependencies
- if everything are done, go to document root and run "`npm test`" to run the unit test
- or, type "`npm start`" to start the api server