'use strict';
var helper = require('../util/helper'); //general tools 
var constant = require('../constants/constants');
var teacherSql = require('../sql/teacherSql');
var studentSql = require('../sql/studentSql');
var relationshipSql = require('../sql/relationshipSql');
var notificationSql = require('../sql/notificationSql');

exports.register = async function (req, res) {
    // to ensure all request body is not empty and in the correct pattern
    if (!req.body.teacher || !req.body.students || !helper.isArray(req.body.students) || !helper.isString(req.body.teacher) ||
        !req.body.teacher != "") {
        return res.status(422).json({ message: constant.missingRequestInfo });
    }
    // check teacher email
    if (!helper.isEmail(req.body.teacher)) {
        return res.status(422).json({ message: req.body.teacher + constant.invalidEmailFormat });
    }
    // check all student email is that valid?
    var invalidEmail = [];
    if (req.body.students.length > 0) {
        for (const student of req.body.students) {
            if (!helper.isEmail(student)) {
                invalidEmail.push(student);
            }
        }
    }
    if (invalidEmail.length > 0) {
        return res.status(422).json({ message: invalidEmail.toString() + " is not a valid email" });
    }
    // start insert data to database
    try {
        // insert teacher
        var teacherId = await teacherSql.tryInsert(req.body.teacher);
        if (teacherId > 0 && req.body.students.length > 0) {
            // insert student
            var studentIdArray = [];

            for (const student of req.body.students) {
                var studentId = await studentSql.tryInsert(student);
                studentIdArray.push(studentId);
            }

            if (studentIdArray.length > 0) {
                await relationshipSql.tryLink(teacherId, studentIdArray);
            }


        }
    } catch (e) {
        console.error(e);
        return res.status(422).json({ message: constant.serverError });
    }
    return res.status(204).json();
};

exports.commonStudents = async function (req, res) {

    if (req.query.teacher) {
        const result = await relationshipSql.getStudentNameByTeacher(req.query.teacher);
        res.status(200).json({ students: result });
    } else {
        res.status(200).json({ students: [] });
    }

};

exports.suspendStudent = async function (req, res) {
    // to ensure all request body is not empty and in the correct pattern
    if (!req.body.student || !helper.isString(req.body.student) || !req.body.student != "") {
        return res.status(422).json({ message: constant.missingRequestInfo });
    }

    if (!helper.isEmail(req.body.student)) {
        return res.status(422).json({ message: req.body.student + constant.invalidEmailFormat });
    }

    try {
        await studentSql.suspendStudent(req.body.student);
    } catch (e) {
        console.error(e);
        return res.status(422).json({ message: constant.serverError });
    }
    return res.status(204).json();
};

exports.retrieveForNotifications = async function (req, res) {
    if (!req.body.teacher || !helper.isString(req.body.teacher) || !req.body.teacher != "" ||
        !req.body.notification || !helper.isString(req.body.notification) || !req.body.notification != "") {
        return res.status(422).json({ message: constant.missingRequestInfo });
    }
    try {
        var sender = req.body.teacher;
        var message = req.body.notification;
        var teacherId = await teacherSql.getTeacherId(sender);
        if(teacherId){
            const result = await notificationSql.sendNotification(sender, message, teacherId);
            return res.status(200).json({ recipients: result });
        }else{
            return res.status(404).json({ message: constant.invalidTeacher });
        }
        
    } catch (e) {
        console.error(e);
        return res.status(422).json({ message: constant.serverError });
    }
};
