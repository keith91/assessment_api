'use strict';
// check is string
exports.isString = function(value) {
    return typeof value === 'string' || value instanceof String;
}

// check is array
exports.isArray = function(value) {
    return value && typeof value === 'object' && value.constructor === Array;
}

// check is email
exports.isEmail = function(value) {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value));
}

// check is email
exports.isAllEmail = async function(value) {
    var boolean = true;
    value.forEach((element, index) => {
        if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(element))){
            boolean = false;
        }
        if(index + 1 == value.length){
            return boolean;
        }
    });
}